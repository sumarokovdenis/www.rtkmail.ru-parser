
from bs4 import BeautifulSoup
from requests import Session
import os
import shutil
from openpyxl import Workbook
from openpyxl.styles import Border
from openpyxl.styles import Side, Alignment

SITE_URL = 'http://www.rtkmail.ru'
user_agent = ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
              "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36")

class Parser:

    def __init__(self):
        self.session = Session()
        self.catalog_dict = {}
        self.cards_dict = {}
        self.session.headers.update({'Accept-Encoding': 'gzip, deflate', 'User-Agent': user_agent})
        self.wb = Workbook()
        self.ws = self.wb.active

    def start_parse(self):
        response = self.session.get(SITE_URL)
        bs = BeautifulSoup(response.text, "html.parser")

        nav_menu = bs.find(recursive=True, attrs={'id': 'topnav'})

        nav_li_elements = nav_menu.findAll('li')

        # collect catalog links and texts

        for li_element in nav_li_elements:
            try:
                a_elements = li_element.findAll('a')
                for a_element in a_elements:
                    a_href = a_element.attrs.get('href')
                    a_text = a_element.text
                    if not self.catalog_dict.get(a_href):
                        self.catalog_dict.update({a_href: a_text})
            except Exception as e:
                print('exception', e)

        # find cards in catalog page

        side = Side(style='medium')
        border = Border(left=side, right=side,
                        top=side, bottom=side)
        column = 1
        row = 1

        for catalog_src in self.catalog_dict:
            try:
                catalog_full_url = SITE_URL + catalog_src
                request = self.session.get(catalog_full_url)
                bs_catalog = BeautifulSoup(request.text)
                ul_catalog = bs_catalog.find('ul', {'class': 'catalog_item'}, recursive=True)
                lis_catalog = ul_catalog.findAll('li')
                for li in lis_catalog:

                    a = li.find('a')
                    a_href = a.attrs.get('href')
                    card_req = self.session.get(SITE_URL + a_href)

                    card_bs = BeautifulSoup(card_req.text)

                    div_body = card_bs.find('div', attrs={'id': 'body'})
                    div_warper = div_body.find('div', {'class': 'warper'})

                    ul_crumbs = div_body.find('ul', attrs={'id': 'crumbs'})
                    lis_crumbs = ul_crumbs.findAll('li')
                    category_row = '/ '
                    for li in lis_crumbs:
                        li_text = li.find('a').text
                        category_row +=li_text + ' / '


                    div_catalog = div_body.find('div', attrs={'id': 'catalog'})
                    h_text = div_catalog.find('h2').text
                    ps_catalog = div_catalog.findAll('p')
                    p_text = ''
                    for p_catalog in ps_catalog:
                        p_text += p_catalog.text.replace('\n', '') + ' '

                    div_catalog_img = div_body.find('div', attrs={'id': 'catalog_img'})
                    catalog_img = div_catalog_img.find('img')
                    img_src = catalog_img.attrs.get('src')
                    img_file_name = os.path.basename(img_src)
                    img_bin = self.session.get((SITE_URL + img_src), stream=True)

                    img_name_path = './images/' + img_file_name

                    with open(img_name_path, 'wb') as out_file:
                        shutil.copyfileobj(img_bin.raw, out_file)
                    del img_bin

                    print(h_text)

                    row_list = [
                        h_text,
                        p_text,
                        category_row,
                        img_name_path
                    ]

                    for column_text in row_list:
                        new_cell = self.ws.cell(row=row, column=column)
                        new_cell.border = border
                        new_cell.value = column_text
                        column += 1

                    column = 1
                    row += 1

            except Exception as e:
                print('sec exception', e)

        self.wb.save('result.xlsx')

if __name__ == '__main__':

    parser = Parser()
    parser.start_parse()
