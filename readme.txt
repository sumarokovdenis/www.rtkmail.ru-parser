Install virtualenv:

 > sudo apt-get install python-virtualenv
 > virtualenv -no-site-packages -p python3 env
 > source ./env/bin/activate
 > pip install -r requirements.txt
 > mkdir images
 > python start.py


